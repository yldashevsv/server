import socket
import asyncio

URLs = {
    "/": "Hello Index",
    "/home": "Hello Home",
    "/favicon.ico": "ico"
}


def parser(request):
    df = request.split(" ")
    method = df[0]
    url = df[1]
    return method, url


def gen_headers(method, url):
    if not method == "GET":
        return "HTTP/1.1 405 Method not allowed\n\n", 405

    if not url in URLs:
        return "HTTP/1.1 404 Not Found\n\n", 404

    return "HTTP/1.1 200 OK\n\n", 200


def gen_content(code, url):
    if code == 404:
        return f"<center><H1>404</H1><p>Not Found <strong>{url}</strong></p></center>"
    if code == 405:
        return "<center><H1>405</H1><p>Method not allowed</p></center>"
    return f"<center><h1>{URLs[url]}</h1></center>"


def generator_respons(request):
    method, url = parser(request)
    headers, code = gen_headers(method, url)
    body = gen_content(code, url)

    return (headers + body).encode()


async def handler(client, addr):
    loop = asyncio.get_event_loop()
    request = (await loop.sock_recv(client, 1024)).decode("UTF-8")
    response = generator_respons(request)
    print(f"Ответ {addr}")
    await loop.sock_sendall(client, response)
    client.close()


async def main():
    print("Start server")
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind(("localhost", 2020))
    server.listen(10)
    server.setblocking(False)

    loop = asyncio.get_event_loop()
    while True:
        client, addr = await loop.sock_accept(server)
        loop.create_task(handler(client, addr))


if __name__ == '__main__':
    asyncio.run(main())
